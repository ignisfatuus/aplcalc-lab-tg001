﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject virus; // sprite for graphing 
    public float initialX;
    public float endX;
    public float graphValue;
    public float xTwoValue;

    // Use this for initialization
    void Start ()
    {
        InvokeRepeating("spawnObject", 0, 0.5f);
        spawnObject();	
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    float calculateGraphValue(float x)
    {
        return x = Mathf.Sin(x);
    }
    public void spawnObject()
    {
        initialX++;

        
        graphValue = calculateGraphValue(initialX);
        xTwoValue = calculateGraphValue(initialX + 1);
       
        Instantiate(virus, new Vector3(initialX, graphValue, 0), Quaternion.identity);
        
    }

}
