﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Antivirus : MonoBehaviour {

    public float endX;
    public Spawner spawnerScript;
    [SerializeField] private float graphValue;
    [SerializeField] private float slope;
    public float speed;
    public float angle;
	// Use this for initialization
	void Start ()
    {
    
        moveToGoal();
    }
	
	// Update is called once per frame
	void Update ()
    {
        moveToGoal();
        graphValue = spawnerScript.graphValue;
    }

    public void moveToGoal()
    {
        slope = spawnerScript.xTwoValue - spawnerScript.graphValue;
        angle = Mathf.Atan(slope) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle,Vector3.forward) ;
        transform.position = Vector2.MoveTowards(transform.position, new Vector3(spawnerScript.initialX,graphValue,5), speed * Time.deltaTime);


    }
}
