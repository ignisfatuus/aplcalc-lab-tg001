rectMode(CENTER);
 
var graphFunction = function(x)

 {
        
return sin(x*10)*10 ; // function of the graph you want

 };

    
    
   
var posX=0;
    
var posY=0;
    
    
draw = function()
   
{
      
var slope = ((100 - graphFunction(posX+1)) - (100 -graphFunction(posX))); // x2-x1 ( slope of the function ) 
       var angle = atan(slope);
 
posX +=1.5; // speed of x increment
  
posY = 100 - graphFunction(posX); //position of y
      
pushMatrix(); // function needed for translation (start)
       
translate(posX,posY); // move rect
      
rotate(angle); // rotate rect with the converted angle 
   
rect(0,0,10,10); // shape of the rect
       
       
popMatrix(); // function needed for translation (end)
       
       
 
};

