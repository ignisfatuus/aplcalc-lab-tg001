﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimation : MonoBehaviour {

    // Use this for initialization
    public GameObject Camera;
    public Animator AnimatorComponent;
    public string AnimatorBool;
	void Start () {
        Camera = GameObject.Find("Main Camera");
        AnimatorComponent = Camera.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (collision.tag == "Player") StartCoroutine(playAnimation());
    }

    IEnumerator playAnimation()
    {
        AnimatorComponent.SetBool(AnimatorBool, true);
        AnimatorComponent.applyRootMotion = false;

        yield return new WaitForSeconds(2);
        AnimatorComponent.SetBool(AnimatorBool, false);
        AnimatorComponent.applyRootMotion = true;
    }
}
