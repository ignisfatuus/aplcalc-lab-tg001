﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    // Use this for initialization
    public float TeleportX;
    public float TeleportY;
    public float TeleportZ;
    private Vector3 TeleportPosition;
	void Start () {
        TeleportPosition = new Vector3(TeleportX, TeleportY, TeleportZ);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag =="Player")
        {
            collision.gameObject.transform.position = new Vector3(TeleportPosition.x,TeleportPosition.y,TeleportPosition.z);
        }
    }
}
