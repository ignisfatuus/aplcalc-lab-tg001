﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

    public Rigidbody2D rb2d;
    public float fallForce;
    public float JumpForce;
    public float VelocityInY;
	// Use this for initialization
	void Start ()
    {
        rb2d = gameObject.GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        VelocityInY = rb2d.velocity.y;
        if (rb2d.velocity.y < 0 && !Input.GetButton("Jump"))
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallForce - 1) * Time.deltaTime;
        }
        else if (rb2d.velocity.y > 0 && !Input.GetButton("Jump"))
        {

            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (JumpForce - 1) * Time.deltaTime;
        }
    }

   

        }
