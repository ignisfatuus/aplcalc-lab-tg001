﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

    public DialogueManager DManager;
    public bool isTriggered;
	// Use this for initialization
	void Start ()
    {
        isTriggered = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" )
        {
            if (isTriggered) return;
            DManager.setDialogueActive();
            isTriggered = true;
        }
    }

}
 