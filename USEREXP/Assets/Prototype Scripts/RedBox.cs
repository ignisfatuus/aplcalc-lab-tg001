﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBox : MonoBehaviour {

    // Use this for initialization
    SpriteRenderer spriteR;
    public Sprite RedBoxSprite;
    public string BulletTag;
	void Start () {
        spriteR = gameObject.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == BulletTag)
        {
            Debug.Log("pls");
            gameObject.GetComponent<Collider2D>().isTrigger = false;
            spriteR.sprite = RedBoxSprite;
      
        }
    }
}
