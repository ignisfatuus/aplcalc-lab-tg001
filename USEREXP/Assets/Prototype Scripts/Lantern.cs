﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lantern : MonoBehaviour {

    // Use this for initialization
    public PlayerShoot PlayerShootScript;
    public GameObject Redbox1;
    public GameObject Redbox2;
    public GameObject Effect;
    public int x = 1;
    public int y = 1;
	void Start ()
    {
        PlayerShootScript = GameObject.Find("Player").GetComponent<PlayerShoot>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && x == 1)
        {
            Debug.Log("gagana");
            if (PlayerShootScript.colorEquipped == 0 && y==1)
            {
                PlayerShootScript.colorEquipped = 1;
                y += 1;
            }
            PlayerShootScript.maxColor = PlayerShootScript.maxColor + 1;
            x += 1;
            Destroy(Redbox1);
            Destroy(Redbox2);
            Instantiate(Effect);
            Effect.transform.position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y + 0.15f);
            
        }
    }
}
