﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TextTrigger : MonoBehaviour {

    // Use this for initialization
    public Text TextTutorial;
    public int Count;
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (Count == 0)
            {
                TextTutorial.text = "Press Space to Jump!";
                StartCoroutine(ShowTextForSeconds());
               
            }
            if (Count==1)
            {
           
                TextTutorial.text = "Press X to Shoot!";
                StartCoroutine(ShowTextForSeconds());
            }
            if(Count==2)
            {
            
                TextTutorial.text = "Press R to switch colors!";
                StartCoroutine(ShowTextForSeconds());
            }
            Count += 1000;
        }


    }
    IEnumerator ShowTextForSeconds()
    {
        TextTutorial.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        TextTutorial.gameObject.SetActive(false);
    }
}
