﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public int Direction;
    public float Speed;
    public float JumpForce;
    public GameObject RedBullet;
    public GameObject BlueBullet;
    public GameObject GreenBullet;
    public float BulletInstantiationDistance;
    Rigidbody2D rb2d;

    public bool facingRight = true;
    public bool isGrounded;
    public float gravity;
    public GameObject door;
    public GameObject key;
    public int colorEquipped;
    public int maxColor;
    public UiSwitch UiSwitchGO;

    public GameObject Camera;
    public Animator AnimatorGO;
    // Use this for initialization
    void Start()
    {
        AnimatorGO = GetComponent<Animator>();
        maxColor = 0;
        colorEquipped = 0;
        Direction = 1;

        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {


     
        Fire();
        gunSwitch();
    }

    //void HorizontalMovement()
    //{
    //    float horizontalInput = Input.GetAxis("Horizontal");
    //    Vector2 moveVelocity = rb2d.velocity;
    //    moveVelocity.x = horizontalInput * Speed;
    //    rb2d.velocity = moveVelocity;
  
    //    if (horizontalInput < 0 && facingRight) Flip();
    //    if (horizontalInput > 0 && !facingRight) Flip();
    //}

    //void JumpMovement()
    //{
    //    if (Input.GetButtonDown("Jump") && isGrounded)
    //    {
    //        rb2d.velocity += JumpForce * Vector2.up;
    //        AnimatorGO.SetBool("isJumping", true);
    //        isGrounded = false;                                                                 
    //    }
    //    else if (!isGrounded)
    //    {
    //        AnimatorGO.SetBool("isJumping", false);
    //        rb2d.velocity -= gravity * Vector2.up;
    //    }
    //}

    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (colorEquipped == 0) return;
            if(colorEquipped==1) Instantiate(RedBullet, new Vector3(gameObject.transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            if(colorEquipped==2) Instantiate(GreenBullet, new Vector3(gameObject.transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            if(colorEquipped==3) Instantiate(BlueBullet, new Vector3(gameObject.transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        }



    }

    //void setDirections()
    //{

    //    if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
    //    {
    //        Direction = -1;
    //    }
    //    else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
    //    {
    //        Direction = 1;
    //    }
        
    //}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Monster")
        {

            Debug.Log("Dead");
            gameObject.transform.position = new Vector3(-6.51f, 0.78f, -6);
            Camera.transform.position = new Vector3(-6.51f, 0.78f, -10);

        }
        if (collision.gameObject.tag == "Acid")
        {

            Debug.Log("Dead");
            gameObject.transform.position = new Vector3(-8.66f, -3.762404f, 0);

        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Key")
        {

            Debug.Log("Game Done");
            Destroy(door);
            Destroy(key);
            //Text.SetActive(true);

        }

        if (collision.gameObject.tag == "Ground" && gameObject.transform.position.y >= collision.gameObject.transform.position.y)
        {
            isGrounded = true;
        }

        if (collision.gameObject.tag =="dead")
        {
            gameObject.transform.position = new Vector3(-1.98f, 0.107f, 0);
        }
    }
    //void Flip()
    //{
    //    facingRight = !facingRight;
    //    transform.Rotate(Vector3.up * 180);
    //}
    void gunSwitch()
        {
        if (maxColor == 0) return;
        if (Input.GetKeyDown(KeyCode.R))
        {
            colorEquipped += 1;
            //UiSwitchGO.RotateSwitcher();
        }
        if (colorEquipped > maxColor) colorEquipped = 1;

        }

    //void RunningAnimation()
    //{
    //    AnimatorGO.SetBool("isRunning", true);
    //}
}
