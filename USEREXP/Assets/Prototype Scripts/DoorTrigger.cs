﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{

    // Use this for initialization
    public GameObject DoorToBeDestroyed;
    public GameObject DoorReplacement;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Vector3 oldDoorPos = DoorToBeDestroyed.transform.position;
            Vector3 oldDoorScale = DoorToBeDestroyed.transform.localScale;
            Destroy(DoorToBeDestroyed);
            Instantiate(DoorReplacement, oldDoorPos, Quaternion.identity);
  

        }


    }
}
