﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acid : MonoBehaviour {

    // Use this for initialization
    SpriteRenderer spriteR;
    public Sprite Water;
    void Start () {
        spriteR = gameObject.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Blue Ammo")
        {
            gameObject.tag = "Water";
            spriteR.sprite = Water;
        }
    }
}
