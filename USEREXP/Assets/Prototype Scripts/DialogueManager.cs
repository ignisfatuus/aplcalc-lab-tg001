﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogueManager : MonoBehaviour {

    // Use this for initialization
    public GameObject Dialogue;
    public PlayerWalk PlayerWalkComponent;
    public PlayerJump PlayerJumpComponent;
    public Text Name;
    public Text DialogueText;
    public int x;
    [TextArea(3,10)]
    public string[] Sentences;

	void Start ()
    {
        PlayerWalkComponent = GameObject.Find("Player").GetComponent<PlayerWalk>();
        PlayerJumpComponent = GameObject.Find("Player").GetComponent<PlayerJump>();
        x = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        ShowText();
	}


    
    void ShowText()
    {
        
        DialogueText.text = Sentences[x];
    }

    public void NextDialogue()
    {
        x += 1;
        setDialogueInactive();
    }

    public void setDialogueActive()
    {
        PlayerWalkComponent.Speed = 0;
        PlayerJumpComponent.JumpForce = 0;
        Dialogue.SetActive(true);
    }

    void setDialogueInactive()
    {

     
        if (x == 3)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }
        if (x == 5)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }
        if (x == 7)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }
        if (x == 9)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }
        if (x == 11)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }
        if (x == 13)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }
       
        if (x == 14)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }


        if (x == 16)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }

        if (x == 17)
        {
            Dialogue.SetActive(false);
            PlayerWalkComponent.Speed = 6;
            PlayerJumpComponent.JumpForce = 8;
        }


    }
}
