﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBehavior : MonoBehaviour {

    public float Speed;
    public float Direction;
    public PlayerWalk PlayerControllerScript;
    public CameraController Camera;
	// Use this for initialization
	void Start ()
    {
        Direction = PlayerControllerScript.Direction;
        Camera = FindObjectOfType<CameraController>();
    }
	
	// Update is called once per frame
	void Update () {
        BulletMovement();
    }

    void BulletMovement()
    {
     
        if(Direction>0)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x+5f,transform.position.y,transform.position.z), Speed * Time.deltaTime);
            //move right
        }
        else if (Direction <0)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x - 5f, transform.position.y, transform.position.z), Speed * Time.deltaTime);
            //move right
        }
        else if (Direction == 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y + 5f, transform.position.z), Speed * Time.deltaTime);
            //move right
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Red Box")
        {
            Destroy(gameObject);

        }
        if (collision.gameObject.tag == "Blue Box")
        {
            Destroy(gameObject);

        }
        if (collision.gameObject.tag == "Monster")
        {

            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
        }
        if (collision.gameObject.tag == "Door")
        {
            Destroy(gameObject);
        }

    }
   
}
