﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TutorialTriggers : MonoBehaviour {

    int Count;
    private PlayerController PlayerControls;
    private Rigidbody2D PlayerRigid;
    public Text TextTutorial;
    public bool hasMoved;
    // Use this for initialization
    void Start () {
        hasMoved = false;
        Count = 0;
        PlayerRigid = GameObject.Find("Player").GetComponent<Rigidbody2D>();

	}
	
	// Update is called once per frame
	void Update () {
        if (Count == 0) StartCoroutine("ShowMovementControls");
        if (PlayerRigid.velocity.x > 0)
        {
            hasMoved = true;

        }
    }

    IEnumerator ShowMovementControls()
    {
        if (PlayerRigid.velocity.x == 0 )
        {
          
       
        }
    
        yield return new WaitForSeconds(3);
        if (!hasMoved) StartCoroutine(ShowTextForSeconds());
        Count += 1;
    }

    void CheckIfMoved()
    {
       
    }

    IEnumerator ShowTextForSeconds()
    {
        TextTutorial.text = "Press A or D to Move!";
        TextTutorial.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        TextTutorial.gameObject.SetActive(false);
    }
}
