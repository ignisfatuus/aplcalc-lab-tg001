﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBooster : MonoBehaviour {

    public PlayerController Player;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void increaseJump()
    {
        Player.JumpForce += 1.25f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("gagana");
            Destroy(gameObject);
            increaseJump();
        }
    }
}
