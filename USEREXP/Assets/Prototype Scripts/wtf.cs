﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wtf : MonoBehaviour
{
    public GameObject Player;
    public Animator AnimatorComponent;
    public float zoomSize;
    public float xLeft;
    public float xRight;
    public float yDown;
    public float yUp;
    
    // Use this for initialization
    void Start()
    {
        AnimatorComponent = gameObject.GetComponent<Animator>();
        StartCoroutine(shakeAtFirst());
       
      
    }

    // Update is called once per frame
    void Update()
    {
        followCamera();
       
    
    }




    void followCamera()
    {


        if (Player.transform.position.x >= xLeft && Player.transform.position.x <= xRight)
        {
            this.transform.position = new Vector3(Player.transform.position.x, this.transform.position.y, -10f);
        }

        if (Player.transform.position.y >= yDown && Player.transform.position.y <= yUp)
        {
            this.transform.position = new Vector3(this.transform.position.x, Player.transform.position.y, -10f);
        }



    }

    void playAnimations()
    {

    }

    IEnumerator shakeAtFirst()
    {
        AnimatorComponent.SetBool("isCameraShaking", true);
        AnimatorComponent.applyRootMotion = false;

        yield return new WaitForSeconds(1.5f);
        AnimatorComponent.SetBool("isCameraShaking", false);
        AnimatorComponent.applyRootMotion = true;
        this.transform.position = new Vector3(Player.transform.position.x+5f, this.transform.position.y, -10f);
    }
}
