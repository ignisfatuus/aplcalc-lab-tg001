﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBehavior : MonoBehaviour
{

    // Use this for initialization
    public float LeftCoordinate;
    public float RightCoordinate;
    public float DistanceCovered;
    public bool isGoingBack;
    public float Speed;
    public GameObject Text;
    public float Team;
    // Use this for initialization
    void Start()
    {

   
    }

    // Update is called once per frame
    private void Update()
    {
        Movement();
    }
    void Movement()
    {
        if (!isGoingBack)
        {
            if (gameObject.transform.position.x >= LeftCoordinate)
            {

                transform.Translate(-Speed, 0, 0);

            }
            else
            {

                isGoingBack = true;
            }
        }


        if (isGoingBack)
        {
            transform.Translate(Speed, 0, 0);
            if (gameObject.transform.position.x >= RightCoordinate)
            {
                isGoingBack = false;
            }
       
        }


    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Blue Ammo" && Team == 0)
        {

            Debug.Log("Monster Dead");

            DestroyObject(gameObject);
            //Text.SetActive(true);
        }
        if (collision.gameObject.tag == "Red Ammo" && Team == 1)
        {

            Debug.Log("Monster Dead");

            DestroyObject(gameObject);
            //Text.SetActive(true);
        }
        if (collision.gameObject.tag == "Green Ammo" && Team == 2)
        {

            Debug.Log("Monster Dead");

            DestroyObject(gameObject);
            //Text.SetActive(true);
        }
    }

}
