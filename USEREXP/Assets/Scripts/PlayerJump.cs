﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour {

    public bool isGrounded;
    public float JumpForce;
    public float gravity;
    public Animator AnimatorComponent;
    public Rigidbody2D rb2d;

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        AnimatorComponent = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        JumpMovement();
	}

    void JumpMovement()
    {
        checkIfGrounded();
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            rb2d.velocity += JumpForce * Vector2.up;
       
            isGrounded = false;
        }
        else if (!isGrounded)
        {
            
     
            rb2d.velocity -= gravity * Vector2.up;
        }
    }

    void checkIfGrounded()
    {
        if (rb2d.velocity.y > 1)
        {
            AnimatorComponent.SetBool("isJumping", true);
            isGrounded = false;
        }

        else if (rb2d.velocity.y<=0)
        {
            AnimatorComponent.SetBool("isJumping", false);
            isGrounded = true;
        }
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {


        if (collision.gameObject.tag == "Ground" && gameObject.transform.position.y >= collision.gameObject.transform.position.y)
        {
            isGrounded = true;
        }
        if (collision.gameObject.tag == "Bridge" && gameObject.transform.position.y >= collision.gameObject.transform.position.y)
        {
            isGrounded = true;
        }

    }
}
