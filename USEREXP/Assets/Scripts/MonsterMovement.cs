﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement : MonoBehaviour {

    public float MoveSpeed = 1.0f;
    public float Team;
    public Transform Player;
    
    // Use this for initialization
    void Start () {
        transform.position = transform.position;
        Player = GameObject.Find("Player").GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector3 (Player.gameObject.transform.position.x, transform.position.y, -15.65f), MoveSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Blue Ammo" && Team == 0)
        {

            Debug.Log("Monster Dead");
         
            DestroyObject(gameObject);
            //Text.SetActive(true);
        }
        if (collision.gameObject.tag == "Red Ammo" && Team == 1)
        {

            Debug.Log("Monster Dead");

            DestroyObject(gameObject);
    //Text.SetActive(true);
}
        if (collision.gameObject.tag == "Green Ammo" && Team == 2)
        {

            Debug.Log("Monster Dead");

            DestroyObject(gameObject);
            //Text.SetActive(true);
        }
    }
}



