﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalk : MonoBehaviour {

    public float Speed;
    public float horizontalDirection;
    public float Direction;
    [SerializeField]private bool isFacingRight;
    private Rigidbody2D rb2d;
    private Animator animatorComponent;
	// Use this for initialization
	void Start ()
    {
        isFacingRight = true;
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        animatorComponent = gameObject.GetComponent<Animator>();
	}

    // Update is called once per frame
    void Update()
    {
        HorizontalMovement();

    }

    void HorizontalMovement()
    {
        horizontalDirection = Input.GetAxis("Horizontal");
        Vector2 moveVelocity = rb2d.velocity;
        moveVelocity.x = horizontalDirection * Speed;
        rb2d.velocity = moveVelocity;
        animatorComponent.SetBool("isRunning", true);

        RunningAnimation();
        if (horizontalDirection < 0 && isFacingRight)
        {
            Flip();
            Direction = -1;
        }
        if (horizontalDirection > 0 && !isFacingRight)
        {
            Direction = 1;
            Flip();
       
        }

    }

    void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(Vector3.up * 180);
    }
    void RunningAnimation()
    {
        if (rb2d.velocity.x == 0) animatorComponent.SetBool("isRunning", false); 
        else animatorComponent.SetBool("isRunning", true);
    }
}
