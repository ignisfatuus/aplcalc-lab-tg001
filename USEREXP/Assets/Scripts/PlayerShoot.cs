﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

    // Use this for initialization
    public GameObject[] Bullets;
    private Animator AnimatorComponent;
    public int colorEquipped;
    public int maxColor;
    public bool isShooting;
    public UiSwitch UiSwitchGO;
	void Start ()
    {
        AnimatorComponent = gameObject.GetComponent<Animator>();
        colorEquipped = 0;
        maxColor = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Fire();
        gunSwitch();
	}

    void Fire()
    {
   
        if (Input.GetKeyDown(KeyCode.X))
        {
          StartCoroutine(setShootingAnimation());
          Instantiate(Bullets[colorEquipped], new Vector3(gameObject.transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        }
    }
    void gunSwitch()
    {
        if (maxColor == 0) return;
        if (Input.GetKeyDown(KeyCode.R))
        {
            colorEquipped += 1;
            UiSwitchGO.RotateSwitcher();
        }
        if (colorEquipped > maxColor) colorEquipped = 1;

    }

    IEnumerator setShootingAnimation()
    {
        isShooting = true;
        AnimatorComponent.SetBool("isThrowing", true);
        yield return new WaitForSeconds(0.5f);
        isShooting = false;
        AnimatorComponent.SetBool("isThrowing", false);
    }
}
