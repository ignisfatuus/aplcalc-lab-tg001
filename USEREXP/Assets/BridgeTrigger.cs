﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeTrigger : MonoBehaviour {

    public GameObject Bridge;
    public GameObject Water;
    public float Limit1;
    public float Limit2;
    public float movementPerSecond;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag =="Player")
        {
            StartCoroutine(MoveObject());
        }
    }

    IEnumerator MoveObject()
    {
        
            while (Bridge.gameObject.transform.position.y > Limit1 && Water.gameObject.transform.position.y > Limit2)
            {
                Bridge.gameObject.transform.position = new Vector2(Bridge.gameObject.transform.position.x, Bridge.gameObject.transform.position.y - movementPerSecond);
                Water.gameObject.transform.position = new Vector2(Water.gameObject.transform.position.x, Water.gameObject.transform.position.y - movementPerSecond);
                yield return new WaitForSeconds(movementPerSecond);
            }
        
    }
}
