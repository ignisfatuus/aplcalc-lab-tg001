﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TankBehavior : MonoBehaviour {

    public float currentHealth;
    public float maxHealth;
    public bool isAlive;
    public SpriteRenderer Tank;
    public SpriteRenderer Water;
    public GameObject[] Monsters;
    public Sprite BlueTank;
    public Sprite BlueWater;
    public DialogueManager DManager;
    public Text EndGameText;
    // Use this for initialization
    void Start ()
    {
     
        Tank = GameObject.Find("WaterTank").GetComponent<SpriteRenderer>();
        Water  = GameObject.Find("AsidoTubeg").GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        checkDeath();
	}

    void TakeDamage()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Blue Ammo")
        {
            currentHealth -=10;
            Destroy(collision);
        }
    }

    void checkDeath()
    {
        if(currentHealth<=10)
        {
            EndGameText.gameObject.SetActive(true);
            Tank.sprite = BlueTank;
            Water.sprite = BlueWater;
            DManager.setDialogueActive();
        }
    }

}
