﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class colorEqupped : MonoBehaviour {

    public Text ColorEquippedText;
    public PlayerShoot PlayerShootComp;
	// Use this for initialization
	void Start ()
    {
        PlayerShootComp = GameObject.Find("Player").GetComponent<PlayerShoot>();
	}
	
	// Update is called once per frame
	void Update () {
        ColorEquipped();
	}

    void ColorEquipped()
    {
        if (PlayerShootComp.colorEquipped == 0)
        {
            ColorEquippedText.text = ("Color Equipped:None");
    
        }
        if (PlayerShootComp.colorEquipped == 1)
        {
            ColorEquippedText.text = ("Color Equipped:Red");
            ColorEquippedText.color = new Vector4(255, 0, 0, 255);
        }
        if (PlayerShootComp.colorEquipped == 2)
        {
            ColorEquippedText.text = ("Color Equipped:Green");
            ColorEquippedText.color = new Vector4(0, 255, 0, 255);
        }
        if (PlayerShootComp.colorEquipped == 3)
        {
            ColorEquippedText.text = ("Color Equipped:Blue");
            ColorEquippedText.color = new Vector4(0, 0, 255, 255);

        }




    }
}
