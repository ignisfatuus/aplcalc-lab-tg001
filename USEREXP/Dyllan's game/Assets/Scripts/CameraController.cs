﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    public GameObject Player;
    public float xLeft;
    public float xRight;
    public float yDown;
    public float yUp;
    // Use this for initialization
    void Start () {
       
    }
	
	// Update is called once per frame
	void Update ()
    {
        followCamera();

    }

    void followCamera()
    {


        if (Player.transform.position.x >= xLeft && Player.transform.position.x <= xRight)
        {
            this.transform.position = new Vector3(Player.transform.position.x, this.transform.position.y, -10f);
        }    

        if (Player.transform.position.y >= yDown && Player.transform.position.y <= yUp)
        {
            this.transform.position = new Vector3(this.transform.position.x, Player.transform.position.y, -10f);
        }

        
        
    }
}
