﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    public int ScoreCount;
	// Use this for initialization
	void Start () {
        ScoreCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void addScore(int increment)
    {
        ScoreCount += 1;
    }
}
