﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public int Direction;
    public float Speed;
    public float JumpForce;
    public float fallForce;
    public GameObject Bullet;
    public float BulletInstantiationDistance;
    Rigidbody2D rb2d;
    
    public bool facingRight = true;
    public bool isGrounded;
    public float gravity;
    public GameObject door;
    public GameObject key;
    public GameObject GameOverUI;
    // Use this for initialization
    void Start ()
    {
        Direction = 1;
       
        rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
       
        setDirections();
        HorizontalMovement();
        JumpMovement();
        Fire();

	}

    void HorizontalMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        Vector2 moveVelocity = rb2d.velocity;
        moveVelocity.x = horizontalInput * Speed;
        rb2d.velocity = moveVelocity;

        if (horizontalInput < 0 && facingRight) Flip();
        if (horizontalInput > 0 && !facingRight) Flip();
    }

    void JumpMovement()
    {
        if (rb2d.velocity.y < 0)
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallForce - 1) * Time.deltaTime;
        }
        else if (rb2d.velocity.y > 0 && !Input.GetButton("Jump"))
        {

            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (JumpForce - 1) * Time.deltaTime;
        }

        
    }

    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
           Instantiate(Bullet, new Vector3(gameObject.transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        }
            
            
        
    }

    void setDirections()
    {
      
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Direction = -1;
        }
        else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            Direction = 1;
        }
        /*else if (Input.GetButtonDown("Jump"))
        {
            Direction = 2;
        }
       */
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Monster")
        {

            Debug.Log("Dead");
         
            Destroy(gameObject);
            GameOverUI.SetActive(true);
       
        }
       
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Key")
        {

            Debug.Log("Game Done");
            Destroy(door);
            Destroy(key);
            //Text.SetActive(true);

        }

        if (collision.gameObject.tag == "Ground" && gameObject.transform.position.y >collision.gameObject.transform.position.y)
        {
            isGrounded = true;
        }
    }
        void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(Vector3.up * 180);
    }
}
