﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBehavior : MonoBehaviour {

    public float Speed;
    public int Direction;
    public PlayerController PlayerControllerScript;
    public Score ScoreScript;
    public int fag;
	// Use this for initialization
	void Start ()
    {
        fag = 0;
        ScoreScript = GameObject.Find("Player").GetComponent<Score>();
        Direction = PlayerControllerScript.Direction;

    }
	
	// Update is called once per frame
	void Update () {
        BulletMovement();
    }

    void BulletMovement()
    {
        if(Direction==1)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x+5f,transform.position.y,transform.position.z), Speed * Time.deltaTime);
            //move right
        }
        else if (Direction == -1)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x - 5f, transform.position.y, transform.position.z), Speed * Time.deltaTime);
            //move right
        }
        else if (Direction == 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, transform.position.y + 5f, transform.position.z), Speed * Time.deltaTime);
            //move right
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Monster" && collision.GetComponent<MonsterBehavior>().fag == 0 )
        {
           
            fag += 1;
       
          



        }
    }
}
