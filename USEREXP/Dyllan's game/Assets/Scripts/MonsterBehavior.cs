﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MonsterBehavior : MonoBehaviour
{

    // Use this for initialization
    public float LeftCoordinate;
    public float RightCoordinate;
    public float DistanceCovered;
    public bool isGoingBack;
    public float Speed;
    public GameObject Text;
    public Score ScoreScript;
    public List<GameObject> DialogueList;
    public bool isShootable;
    public int fag=0;
    public bool isSpaniard;
    // Use this for initialization
    void Start()
    {
     

        ScoreScript = GameObject.Find("Player").GetComponent<Score>();
        ScoreScript.ScoreCount /= 2;
        Text.SetActive(false);
        LeftCoordinate = transform.position.x - DistanceCovered+1;
        RightCoordinate = transform.position.x + DistanceCovered;
    }

    // Update is called once per frame
    private void Update()
    {
        Movement();
    }
    void Movement()
    {
        if (!isGoingBack)
        {
            if (gameObject.transform.position.x >= LeftCoordinate)
            {

                transform.Translate(-Speed, 0, 0);

            }
            else
            {

                isGoingBack = true;
            }
        }


        if (isGoingBack)
        {
            transform.Translate(Speed, 0, 0);
            if (gameObject.transform.position.x >= RightCoordinate)
            {
                isGoingBack = false;
            }
        }


    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ammo" && fag==0 && !isSpaniard)
        {
            fag += 1;
            DestroyObject(collision.gameObject);
            StartCoroutine(despawn());
            ScoreScript.addScore(1);



        }
    } 

    IEnumerator despawn()
    {
        int randomNumber = Random.Range(0, 3);
        Instantiate(DialogueList[randomNumber], new Vector2(gameObject.transform.position.x + 1.5f, gameObject.transform.position.y + 1.5f), Quaternion.identity);
        Speed = 0;
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
      
        Text.SetActive(true);
        Debug.Log("betlog");
    }

}
