﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour {

    public float JumpForce;
    public PlayerController PlayerControllerScript;
	// Use this for initialization
	void Start () {
        PlayerControllerScript = GameObject.Find("Player").GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Jump") && PlayerControllerScript.isGrounded)
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.up * JumpForce;
            PlayerControllerScript.isGrounded = false;
        }
	}
}
