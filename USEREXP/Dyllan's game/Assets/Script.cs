﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Script : MonoBehaviour {

    public Text ScoreText;
    public Text VictoryText;
    public Score ScoreScript;

	// Use this for initialization
	void Start () {
        ScoreScript = GameObject.Find("Player").GetComponent<Score>();
        
	}
	
	// Update is called once per frame
	void Update () {
        ScoreText.text = (ScoreScript.ScoreCount) + "/10";
        if(ScoreScript.ScoreCount == 10)
        {
            VictoryText.gameObject.SetActive(true);
        }
	}
}
