#pragma once
class Rectangle
{
public:
public:
	
	Rectangle(float rectangleX, float rectangleY, float rectangleWidth, float rectangleHeight, float rectAnchorX, float rectAnchorY);
	~Rectangle();

	float xPosition;
	float yPosition;

	float width;
	float height;

	float xAnchor;
	float yAnchor;

	float leftPoint;
	float rightPoint;

	float downPoint;
	float upPoint;

	bool isXColliding;
	bool isYColliding;

	bool hasCollided;
	bool isColliding(Rectangle rectangleToCheck);

	void setLeftPoint();
	void setRightPoint();
	void setDownPoint();
	void setUpPoint();

	void checkCollision();

};

