#include "Rectangle.h"
#include <iostream>
#include <stdlib.h>  
#include <math.h> 
using namespace std;




Rectangle::Rectangle(float rectangleX, float rectangleY, float rectangleWidth, float rectangleHeight, float rectAnchorX, float rectAnchorY)
{
	xPosition = rectangleX;
	yPosition = rectangleY;
	width = rectangleWidth;
	height = rectangleHeight;
	xAnchor = rectAnchorX;
	yAnchor = rectAnchorY;

	setLeftPoint();
	setRightPoint();
	setDownPoint();
	setUpPoint();


	
}

Rectangle::~Rectangle()
{
}

bool Rectangle::isColliding(Rectangle rectangleToCheck)
{
	hasCollided = false;
	isXColliding = false;
	isYColliding = false;
	if (leftPoint >= rectangleToCheck.leftPoint && leftPoint <= rectangleToCheck.rightPoint)
	{
		isXColliding = true;
	}

	else if (rightPoint >=rectangleToCheck.leftPoint && rightPoint<= rectangleToCheck.rightPoint)
	{
		isXColliding = true;
	}
	if (downPoint>=rectangleToCheck.downPoint && downPoint<= rectangleToCheck.upPoint)
	{
		isYColliding = true;
	}
	else if (upPoint >= rectangleToCheck.downPoint && upPoint <= rectangleToCheck.upPoint)
	{
		isYColliding = true;
	}
	hasCollided = isXColliding && isYColliding;
	return hasCollided;
	
}

void Rectangle::setLeftPoint()
{
	leftPoint = xPosition - (width*xAnchor);
}

void Rectangle::setRightPoint()
{
	rightPoint = xPosition + (width*xAnchor);
}

void Rectangle::setDownPoint()
{
	downPoint = yPosition - (height*yAnchor);
}

void Rectangle::setUpPoint()
{
	upPoint = yPosition + (height*yAnchor);
}

void Rectangle::checkCollision()
{
	if (hasCollided)
	{
		cout << "They collided man" << endl;
	}
	else
	{
		cout << "They didnt collide man" << endl;
	}
}
