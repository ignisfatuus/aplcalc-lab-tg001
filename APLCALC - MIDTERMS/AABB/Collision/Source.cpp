#include <iostream>
#include "Rectangle.h"
using namespace std;

void inputInfo(float &positionX, float &positionY, float &rectWidth, float &rectHeight, float &anchorX, float &anchorY)
{
	cout << "xPosition:";
	cin >> positionX;

	cout << "YPosition:";
	cin >> positionY;

	cout << "Width:";
	cin >> rectWidth;

	cout << "Height:";
	cin >> rectHeight;

	cout << "xAnchor:";
	cin >> anchorX;

	while (anchorX < 0 || anchorX >1)
	{
		cout << "xAnchor:";
		cin >> anchorX;
		if (anchorX >= 0 && anchorX <= 1)
		{
			break;
		}

	}

	cout << "yAnchor:";
	cin >> anchorY;

	while (anchorY < 0 || anchorY >1)
	{
		cout << "yAnchor:";
		cin >> anchorY;
		if (anchorY >= 0 && anchorY <= 1)
		{
			break;
		}

	}
	cout << endl;
}

int main()
{
	float xPosition=0;
	float yPosition=0;
	float width=0;
	float height=0;
	float xAnchor=0;
	float yAnchor=0;

	cout << "Rectangle A" << endl;
	inputInfo(xPosition, yPosition, width, height, xAnchor, yAnchor);
	Rectangle rectangleA = Rectangle(xPosition, yPosition, width, height, xAnchor, yAnchor);

	cout << "Rectangle B" << endl;
	inputInfo(xPosition, yPosition, width, height, xAnchor, yAnchor);
	Rectangle rectangleB = Rectangle(xPosition, yPosition, width, height, xAnchor, yAnchor);



	if (rectangleA.isColliding(rectangleB))
	{
		cout << "Rectangles Collided" << endl;
	}
	else
	{
		cout << "Rectangles did not collide" << endl;
	}

	system("pause");
	return 0;
}