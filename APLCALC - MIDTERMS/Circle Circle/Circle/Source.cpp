#include "Circle.h"
#include <stdio.h>      
#include <stdlib.h>  
#include <math.h> 
#include <iostream>
using namespace std;

void inputInfo(float &positionX, float &positionY, float &circleRadius)
{

	cout << "xPosition:";
	cin >> positionX;

	cout << "YPosition:";
	cin >> positionY;

	cout << "Radius:";
	cin >> circleRadius;

	cout << endl;
}
int main()
{
	float xPosition;
	float yPosition;
	float radius;

	cout << "Circle A" << endl;
	inputInfo(xPosition, yPosition, radius);
	Circle circleA = Circle(xPosition, yPosition, radius);


	cout << "Circle B" << endl;
	inputInfo(xPosition, yPosition, radius);
	Circle circleB = Circle(xPosition, yPosition, radius);

	if (circleA.CheckIntersection(circleB))
	{
		cout << "Circles intersected" << endl;
	}
	else
	{
		cout << "Circles did not intersect" << endl;
	}

	system("pause");
	return 0;
}
