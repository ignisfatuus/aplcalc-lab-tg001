#pragma once


class Circle
{
public:
	float xPosition;
	float yPosition;
	float radius;

	Circle(float xPos, float yPos, float circleRadius);
	~Circle();
	bool CheckIntersection(Circle circleB);
};

